FROM ubuntu:22.04 as builder

ENV AWSCLI_VERSION=2.7.18
ENV BUNDLER_VERSION=2.3.15
ENV CMAKE_MINOR_VERSION=3.18
ENV CMAKE_VERSION=3.18.1
ENV GIT_VERSION=2.29.0
ENV GO_VERSION=1.18.7
ENV GOLANG_FIPS_TAG=go1.18.7-2-openssl-fips
ENV LICENSE_FINDER_VERSION=6.14.2
ENV NODE_VERSION=16.14.2
ENV PINENTRY_VERSION=1.1.0
ENV RUBYGEMS_VERSION=3.3.22
ENV RUBY_VERSION=3.0.4
ENV RUBY_MINOR_VERSION=3.0
ENV YARN_VERSION=1.22.18
ENV GCLOUD_VERSION=410.0.0

ENV TZ="Etc/UTC"

# Install required packages
RUN apt-get update -q \
  && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      build-essential \
      libevent-2.1-7 \
      wget \
      git \
      libc6-dev \
      cmake \
      autoconf \
      bison \
      libyaml-dev \
      tzdata \
      locales \
      curl \
      ca-certificates \
      gnupg \
      pkg-config \
      gcc \
      file \
      libgmp-dev \
      automake \
      autopoint \
      lsb-core \ 
      software-properties-common
      

    # Omnibus deps, pull in as needed
    #   autoconf \
    #   automake \
    #   autopoint \
    #   zlib1g-dev \
    #   byacc \
    #   git \
    #   gcc \
    #   g++ \
    #   libssl-dev \
    #   libyaml-dev \
    #   libffi-dev \
    #   libreadline-dev \
    #   libgdbm-dev \
    #   libncurses5-dev \
    #   make \
    #   bzip2 \
    #   curl \
    #   libcurl4-openssl-dev \
    #   ca-certificates \
    #   openssh-server \
    #   libexpat1-dev \
    #   gettext \
    #   libz-dev \
    #   fakeroot \
    #   ccache \
    #   distcc \
    #   unzip \
    #   tzdata \
    #   apt-transport-https \
    #   gnupg \
    #   pkg-config \
    #   python3

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN curl -fsSL "https://storage.googleapis.com/golang/go${GO_VERSION}.linux-amd64.tar.gz" \
    | tar -xzC /usr/local \
    && ln -sf /usr/local/go/bin/go /usr/local/go/bin/gofmt /usr/local/go/bin/godoc /usr/local/bin/
RUN curl -L https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}-Linux-x86_64.sh -o /tmp/cmake-installer.sh \
    && chmod +x /tmp/cmake-installer.sh \
    && /tmp/cmake-installer.sh --prefix=/usr --exclude-subdir --skip-license
RUN curl -Lo /tmp/gcloud-cli.tar.gz "https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-${GCLOUD_VERSION}-linux-x86_64.tar.gz" \
  && tar -C/usr/local -xf /tmp/gcloud-cli.tar.gz \
  && /usr/local/google-cloud-sdk/install.sh --quiet --usage-reporting=false --path-update=true
RUN curl -fsSL "https://nodejs.org/download/release/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz" \
    | tar --strip-components 1 -xzC /usr/local/ \
    && node --version
RUN mkdir /usr/local/yarn \
    && curl -fsSL "https://yarnpkg.com/downloads/${YARN_VERSION}/yarn-v${YARN_VERSION}.tar.gz" \
    | tar -xzC /usr/local/yarn --strip 1 \
    && ln -sf /usr/local/yarn/bin/yarn /usr/local/bin/ \
    && yarn --version
RUN curl -fsSL https://packagecloud.io/84codes/crystal/gpgkey | gpg --dearmor | tee /etc/apt/trusted.gpg.d/84codes_crystal.gpg
RUN . /etc/os-release
RUN echo "deb https://packagecloud.io/84codes/crystal/any any main" | tee /etc/apt/sources.list.d/84codes_crystal.list
RUN apt-get update -y && apt-get install -y crystal

FROM ubuntu:22.04

LABEL org.opencontainers.image.authors="GitLab Quality. <quality@gitlab.com>"

COPY --from=builder / /

WORKDIR /usr/src/app

# Build the project
COPY shard.yml shard.lock .
RUN shards install
COPY src src
COPY .git .git
RUN shards build
RUN mv bin/gitlab-builder /

# Copy assets
RUN mkdir -p /opt/barista/resources

COPY ./src/files /opt/barista/resources/files

# Patches and templates should be compiled into the binary.
# COPY ./src/patches /opt/barista/resources/patches
# COPY ./src/templates /opt/barista/resources/templates

WORKDIR /

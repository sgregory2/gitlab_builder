module Gitlab::TaskHelpers
  include PlatformHelper
  
  getter :context

  macro included
    def self.use_system_ssl?
      ENV["USE_SYSTEM_SSL"]? == "true"
    end
  end

  def ruby3_build?
    ENV["RUBY3_BUILD"]? == "true"
  end

  def update_config_guess(target : String? = nil)
  end

  def resources_path
    project.resources_path
  end

  def use_system_ssl?
    self.class.use_system_ssl?
  end

  def gcc_target
    raise "Not Implemented"
  end

  def no_sourcemaps?
    ENV["NO_SOURCEMAPS"]? == "true" 
  end

  def initialize(
    @project : Barista::Behaviors::Omnibus::Project,
    *,
    @callbacks : Barista::Behaviors::Omnibus::CacheCallbacks = Barista::Behaviors::Omnibus::CacheCallbacks.new, 
    @context : TaskData = TaskData.new
  )
    super(project, callbacks)
  end

  def with_cache_dest(hash : Hash(String, String)? = {} of String => String) : Hash(String, String)
    f = with_destdir(hash)
    puts f
    f
  end

  # depends on Ruby being installed.
  def ruby_version(env : Hash(String, String))
    lines = [] of String
    bin("ruby", "-e 'puts RUBY_VERSION.match(/\\d+\\.\\d+/)[0]'", env: env)
      .collect_output(lines)
      .execute

    lines.last.try(&.chomp)
  end

  def is_auto_deploy_tag?
    ENV["CI_COMMIT_REF_NAME"]?.try(&.includes?("-auto-deploy-")) || false
  end
end
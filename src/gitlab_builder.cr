require "barista"
require "./platform_helper"
require "./common"
require "./task_data"
require "./task_helpers"

class GitlabCE < Barista::Project
  include Gitlab::Common

  @@name = "gitlab-ce"

  def initialize
    super

    description("GitLab Community Edition (including NGINX, Postgres, Redis)")
  end
end

class GitlabEE < Barista::Project
  include Gitlab::Common

  @@name = "gitlab-ee"

  def initialize
    super
    
    description("GitLab Enterprise Edition (including NGINX, Postgres, Redis)")
  end

  def build(version : String, workers : Int32, filter : Array(String)?)
    super(version, workers, ee: true)
  end
end

class GitlabFIPS < Barista::Project
  include Gitlab::Common

  @@name = "gitlab-fips"

  def initialize
    super
    
    description("GitLab Enterprise Edition (including NGINX, Postgres, Redis) with FIPS compliance")
    extra_config
  end

  def extra_config
    if rhel?
      runtime_dependency("openssl-perl")
    else
      runtime_dependency("openssl")
    end
  end
end

class GitlabTest < Barista::Project
  include Gitlab::Common

  @@name = "gitlab-test"

  def initialize
    super
    
    description("GitLab Testing Edition")
    install_dir("./test/opt/gitlab")
    barista_dir("./test/opt/barista")
  end

  def build(version : String, workers : Int32, filter : Array(String)?)
    clean! if Dir.exists?(install_dir)

    super(version: version, workers: workers)
    
    packager
      .on_output { |str| puts str }
      .on_error { |str| puts "package error: #{str}" }
      .run

    puts packager.query
    puts packager.list_files
  end
end

require "./tasks/**"
require "./cli_commands/**"

project_map = [GitlabCE, GitlabEE, GitlabFIPS, GitlabTest].reduce({} of String => Barista::Behaviors::Omnibus::Project) do |memo, klass|
  p = klass.new
  memo[p.name] = p
  memo
end

console = ACON::Application.new("gitlab-builder")
console.add(BuildProject.new(project_map))
console.add(ProjectTasks.new(project_map))
console.run
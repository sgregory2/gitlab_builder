struct PrometheusFlags
  getter :common_version, :version
  def initialize
    @common_version = "github.com/prometheus/common/version"
  end

  def ldflags(version) : String
    [
      "-X #{common_version}.Version=#{version}",
      "-X #{common_version}.Branch=master",
      "-X #{common_version}.BuildUser=GitLab-Omnibus",
      "-s",
      "-w"
    ].join(" ")
  end
end

class ProjectTasks < ACON::Command
  include Barista::Behaviors::Software::OS::Information

  @@default_name = "tasks"

  getter :project_map

  def initialize(@project_map : Hash(String, Barista::Behaviors::Omnibus::Project)); 
    super()
  end

  protected def execute(input : ACON::Input::Interface, output : ACON::Output::Interface) : ACON::Command::Status
    edition = input.argument("edition") || "gitlab-ce"
    project = project_map[edition]?

    unless project
      output.puts("<error>#{edition} is not a supported edition<error>")
      return ACON::Command::Status::FAILURE
    end

    output.puts("<info>#{project.tasks.map(&.name).join(", ")}</info>")
    ACON::Command::Status::SUCCESS
  end

  def configure : Nil
    self
      .description("Shows tasks for a GitLab edition")
      .argument("edition", :optional, "the edition to build [#{project_map.keys.join("|")}] (default gitlab-ce)")
  end
end
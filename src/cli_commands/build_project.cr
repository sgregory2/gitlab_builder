class BuildProject < ACON::Command
  include Barista::Behaviors::Software::OS::Information

  @@default_name = "build"

  getter :project_map

  def initialize(@project_map : Hash(String, Barista::Behaviors::Omnibus::Project)); 
    super()
  end

  protected def execute(input : ACON::Input::Interface, output : ACON::Output::Interface) : ACON::Command::Status
    edition = input.argument("edition") || "gitlab-ce"
    workers = input.option("workers", Int32?) || memory.cpus.try(&.-(1)) || 1
    version = input.option("build") || "master"
    filter = input.option("filter").try(&.split(","))

    project = project_map[edition]?

    unless project
      output.puts("<error>#{edition} is not a supported edition<error>")
      return ACON::Command::Status::FAILURE
    end

    begin
      project.build(version: version, workers: workers, filter: filter)
      ACON::Command::Status::SUCCESS
    rescue ex
      output.puts("<error>Build failed: #{ex.message}</error>")
      ACON::Command::Status::FAILURE
    end
  end

  def configure : Nil
    self
      .description("Builds a GitLab edition")
      .argument("edition", :optional, "the edition to build [#{project_map.keys.join("|")}] (default gitlab-ce)")
      .option("build", "b", :optional, "the version of the edition to build (default master)")
      .option("workers", "w", :optional, "The number of concurrent build workers (default #{memory.cpus.try(&.-(1)) || 1})")
      .option("filter", "f", :optional, "A comma delimited list of tasks to filter")
  end
end
require "./version"

struct TaskData
  getter :version, :prometheus_flags

  def initialize(@version : Gitlab::Version = Gitlab::Version.new, @prometheus_flags : PrometheusFlags = PrometheusFlags.new); end
end
require "yaml"

@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::GitlabRails < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-rails"

  dependency PkgConfigLite
  dependency Ruby
  dependency Bundler
  dependency Libxml2
  dependency Libxslt
  dependency Curl
  dependency Rsync
  dependency Libicu
  dependency Postgresql
  dependency PostgresqlNew
  dependency PythonDocutils
  dependency Krb5
  dependency Registry
  dependency Unzip
  dependency Libre2
  dependency Gpgme
  dependency Graphicsmagick
  dependency Exiftool

  # from barista-gitlab
  # dependency FFIGem
  # dependency GemUpdate

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)

    copy "config/gitlab.yml.example", "config/gitlab.yml"
    copy "config/secrets.yml.example", "config/secrets.yml"

    block "render database.yml" do
      yaml = File.open("#{source_dir}/config/database.yml.postgresql") do |file|
        YAML.parse(file)
      end

      unless context.version.ee?
        yaml.as_h.each do |_key, databases|
          databases.as_h.delete("geo")
        end
      end

      File.open("#{source_dir}/config/database.yml", "w") do |file|
        yaml.to_yaml(file)
      end
    end
  end

  def configure : Nil
    version(context.version.gitlab_version)
    source(source_url)
    license("MIT")
    cache(false)
  end

  def source_url
    if context.version.ee?
      "https://gitlab.com/gitlab-org/gitlab/-/archive/#{version}/gitlab-#{version}.tar.gz"
    else
      "https://gitlab.com/gitlab-org/gitlab-foss/-/archive/#{version}/gitlab-foss-#{version}.tar.gz"
    end
  end
end
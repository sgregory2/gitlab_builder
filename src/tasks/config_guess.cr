@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::ConfigGuess < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "config-guess"

  file("license_patch", "#{__DIR__}/../patches/config_guess/add-license-file.patch")

  def build : Nil
    patch(file("license_patch"), string: true)

    mkdir(target_path, parents: true)
    copy("config.guess", target_path)
    copy("config.sub", target_path)
  end

  # TODO: add license file
  def configure : Nil
    version("12.2.0")
    license("GPL-3.0 (with exception)")
    source("https://github.com/gcc-mirror/gcc/archive/refs/tags/releases/gcc-#{version}.tar.gz")
  end

  def target_path
    File.join(smart_install_dir, "embedded", "lib", "config_guess")
  end
end
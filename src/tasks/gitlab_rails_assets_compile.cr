@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::GitlabRailsAssetsCompile < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  dependency GitlabRailsBundle
  dependency GitlabRailsYarn

  @@name = "gitlab-rails-assets-compile"

  # Breaking up raks assets:compile
  # in order to better support a concurrent build
  def build : Nil
    bin("bundle", "exec rake gettext:po_to_json", env: assets_compile_env, chdir: rails_dir)
    bin("bundle", "exec rake rake:assets:precompile", env: assets_compile_env, chdir: rails_dir)
    bin("bundle", "exec rake gitlab:assets:fix_urls", env: assets_compile_env, chdir: rails_dir)
    bin("bundle", "exec rake gitlab:assets:check_page_bundle_mixins_css_for_sideeffects", env: assets_compile_env, chdir: rails_dir)
  end

  def rails_dir
    File.join(project.source_dir, "gitlab-rails")
  end

  def assets_compile_env : Hash(String, String)
    env = { 
      "NODE_ENV" => "production",
      "RAILS_ENV" => "production",
      "PATH" => "#{install_dir}/embedded/bin:#{ENV["PATH"]}",
      "USE_DB" => "false",
      "SKIP_STORAGE_VALIDATION" => "true",
      "NODE_OPTIONS" => "--max_old_space_size=#{max_old_space_size}"
    }
    env["NO_SOURCEMAPS"] = "true" if no_sourcemaps?
    env
  end

  def max_old_space_size
    "10584"
  end

  def configure : Nil
    cache(false)
    virtual(true)
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Redis < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "redis"

  file("extra_config_patch", "#{__DIR__}/../patches/redis/jemalloc-extra-config-flags.patch")

  dependency ConfigGuess
  dependency OpenSSL unless use_system_ssl?

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)

    env["PREFIX"] = File.join(smart_install_dir, "embedded")
    env["CFLAGS"] += " -fno-omit-frame-pointer"
    env["LDFLAGS"] += " -latomic" if raspberry_pi?

    # jemallocs page size must be >= to the runtime pagesize
    # Use large for arm/newer platforms based on debian rules:
    # https://salsa.debian.org/debian/jemalloc/-/blob/c0a88c37a551be7d12e4863435365c9a6a51525f/debian/rules#L8-23
    env["EXTRA_JEMALLOC_CONFIGURE_FLAGS"] = arm64? ? "--with-lg-page=16" : "--with-lg-page=12"

    patch(file("extra_config_patch"), string: true)

    update_config_guess

    args = ["BUILD_TLS=yes"]
    args << "uname_M=armv6l" if raspberry_pi?

    command("make #{args.join(" ")}", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    license("BSD-3-Clause")
    version("6.2.7")
    source("https://download.redis.io/releases/redis-#{version}.tar.gz")
    preserve_symlinks(false)
  end

  # TODO: implement for arm
  # https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/config/software/redis.rb#L34
  def allowlist
  end
end
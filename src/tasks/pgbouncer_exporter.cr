@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::PgbouncerExporter < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "pgbouncer-exporter"

  def build : Nil
    env = {
      "GOPATH" => "#{project.source_dir}/pgbouncer_exporter",
      "GO111MODULE" => "on"
    }

    command("go build -ldflags '#{context.prometheus_flags.ldflags(version)}'", env: env)
    mkdir(target, parents: true)
    copy("pgbouncer_exporter", target)

    license_finder
  end

  # TODO: implement license finder
  def license_finder
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def configure: Nil
    license("MIT")
    version("0.5.1")
    relative_path("src/github.com/prometheus-community/pgbouncer_exporter")
    source("https://github.com/prometheus-community/pgbouncer_exporter/archive/refs/tags/v#{version}.tar.gz")
  end
end
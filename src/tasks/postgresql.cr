@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Postgresql < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "postgresql"

  file("no_docs_patch", "#{__DIR__}/../patches/postgresql/no_docs.patch")

  dependency Zlib
  dependency OpenSSL unless use_system_ssl?
  dependency Libedit
  dependency Ncurses
  dependency LibosspUuid
  dependency ConfigGuess

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    env["CFLAGS"] += " -fno-omit-frame-pointer"

    update_config_guess(target: "config")
    patch(file("no_docs_patch"), target: "GNUmakefile.in", plevel: 0, string: true)

    command([
      "./configure",
      "--prefix=#{prefix}",
      "--with-libedit-preferred",
      "--with-openssl",
      "--with-uuid=ossp"
    ].join(" "), env: env)

    command("make world", env: env)
    command("make install world", env: env)
  end

  def prefix
    File.join(install_dir, "embedded", "postgresql", major_version)
  end

  def major_version
    "12"
  end

  def configure: Nil
    license("PostgreSQL")
    version("12.12")
    source("https://ftp.postgresql.org/pub/source/v#{version}/postgresql-#{version}.tar.bz2",
      sha256: "34b3f1c69408e22068c0c71b1827691f1c89153b0ad576c1a44f8920a858039c")


    # exclude headers and static libraries from package
    project.exclude("embedded/postgresql/#{major_version}/include")
    project.exclude("embedded/postgresql/#{major_version}/lib/*.a")
    project.exclude("embedded/postgresql/#{major_version}/lib/pgxs")
    project.exclude("embedded/postgresql/#{major_version}/lib/pkgconfig")
  end
end
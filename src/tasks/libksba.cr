@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Libksba < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libksba"

  dependency LibgpgError

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    command("./configure --prefix=#{install_dir}/embedded --disable-doc", env: env)
    command("make -j 3", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    version("1.4.0")
    license("LGPL-3")
    source("https://www.gnupg.org/ftp/gcrypt/libksba/libksba-#{version}.tar.bz2",
      sha256: "bfe6a8e91ff0f54d8a329514db406667000cb207238eded49b599761bfca41b6")
    project.exclude("embedded/bin/ksba-config")
  end
end
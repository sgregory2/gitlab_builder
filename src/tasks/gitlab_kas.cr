@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::GitlabKas < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-kas"

  def build : Nil
    env = { "TARGET_DIRECTORY" => "#{source_dir}/build" }
    mkdir(bin_dir, parents: true)

    command("make kas", env: env)
    command("mv build/kas #{bin_dir}/gitlab-kas")

    find_licenses
  end

  # TODO: use a License finder
  # https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/config/software/gitlab-kas.rb#L38
  def find_licenses
  end

  def bin_dir
    File.join(smart_install_dir, "embedded", "bin")
  end

  def configure : Nil
    license("MIT")
    version(context.version.gitlab_kas_version)
    source("https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/archive/#{version}/gitlab-agent-#{version}.tar.gz")
  end
end
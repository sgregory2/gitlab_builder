@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::GitlabExporter < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-exporter"

  file("license_patch", "#{__DIR__}/../patches/gitlab-exporter/add-license-file.patch")

  dependency Ruby
  dependency PostgresqlNew
  sequence ["Gem"]

  def build : Nil
    patch(file("license_patch"), string: true)

    env = with_standard_compiler_flags(with_embedded_path)
    bin("gem", "install gitlab-exporter --no-document --version #{version}", env: env)
  end

  def configure : Nil
    license("MIT")
    version("12.0.1")
    cache(false)
  end
end
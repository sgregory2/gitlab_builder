@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Git < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "git"

  dependency Zlib
  dependency OpenSSL unless use_system_ssl?
  dependency Curl
  dependency Pcre2
  dependency Libiconv

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    opts = build_options

    if use_system_ssl?
      env["FIPS_MODE"] = "1"
    else
      opts << "GIT_APPEND_BUILD_OPTIONS += OPENSSLDIR=#{install_dir}/embedded"
    end

    if is_auto_deploy_tag?
      git_repo_url.try do |v|
        env["GIT_REPO_URL"] = v
      end

      gitaly_git_2_37_1.try do |v|
        env["GIT_VERSION_2_37_1"] = v
      end

      gitaly_git_2_38.try do |v|
        env["GIT_VERSION_2_38"] = v
      end
    # matching on runtime gitlab version (omnibus gets the version from environment?)
    elsif self_managed_git_override.try(&.matches?(context.version.gitlab_version))
      git_repo_url.try do |v|
        env["GIT_REPO_URL"] = v
      end
    end

    block do
      File.open(File.join(source_dir, "config.mak"), "a") do |file|
        file.print opts.join("\n")
      end
    end

    # From Omnibus GitLab
    # -------------------
    # For now we install both a Git distribution as well as bundled Git. This is
    # only done temporarily to migrate to bundled Git via a feature flagged
    # rollout. Eventually, we will only install bundled Git.
    command("make git install-bundled-git PREFIX=#{install_dir}/embedded GIT_PREFIX=#{install_dir}/embedded", env: env)
  end

  def self_managed_git_override : Regex?
    if txt = ENV["SELF_MANAGED_VERSION_REGEX_OVERRIDE_GIT_REPO_URL"]?
      Regex.new(txt)
    else 
      nil
    end
  end

  def git_repo_url : String?
    ENV["GITALY_GIT_REPO_URL"]?
  end

  def gitaly_git_2_37_1
    ENV["GITALY_GIT_VERSION_2_37_1"]?
  end

  def gitaly_git_2_38
    ENV["GITALY_GIT_VERSION_2_38"]?
  end

  def build_options
    git_cflags = "-fno-omit-frame-pointer"

    # CentOS 7 uses gcc v4.8.5, which uses C90 (`-std=gnu90`) by default.
    # C11 is a newer standard than C90, and gcc v5.1.0 switched the default
    # from `std=gnu90` to `std=gnu11`.
    # Git v2.35 added a balloon test that will fail the build if
    # C99 is not supported. On other platforms, C11 may be required
    # (https://gitlab.com/gitlab-org/gitlab-git/-/commit/7bc341e21b5).
    # Similar is the case for SLES OSs also.
    if get_centos_version == "7" || os_platform == "sles"
      git_cflags += " -std=gnu99"
    end

    [
      "# Added by gitlab_builder git software definition git.cr",
      "GIT_APPEND_BUILD_OPTIONS += CURLDIR=#{install_dir}/embedded",
      "GIT_APPEND_BUILD_OPTIONS += ICONVDIR=#{install_dir}/embedded",
      "GIT_APPEND_BUILD_OPTIONS += ZLIB_PATH=#{install_dir}/embedded",
      "GIT_APPEND_BUILD_OPTIONS += NEEDS_LIBICONV=YesPlease",
      "GIT_APPEND_BUILD_OPTIONS += NO_R_TO_GCC_LINKER=YesPlease",
      "GIT_APPEND_BUILD_OPTIONS += INSTALL_SYMLINKS=YesPlease",
      "GIT_APPEND_BUILD_OPTIONS += CFLAGS=\"#{git_cflags}\"",
      "GIT_APPEND_BUILD_OPTIONS += OPENSSLDIR=#{install_dir}/embedded"
    ]
  end

  # TODO: add license file
  def configure : Nil
    # support vendor
    # vendor("gitlab")
    license("GPL-2.0")
    # license_file("_build/deps/git/source/COPYING")
    version(context.version.gitaly_version)
    source("https://gitlab.com/gitlab-org/gitaly/-/archive/#{version}/gitaly-#{version}.tar.gz")
  end
end
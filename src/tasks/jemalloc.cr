@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Jemalloc < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "jemalloc"

  dependency Redis

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)

    if centos6?
      command("sed -i -e s:autoconf:autoconf268: autogen.sh", env: env)
      env["AUTOCONF"] = "/usr/bin/autoconf268"
    end

    autogen_command = [
      "./autogen.sh",
      "--enable-prof",
      "--prefix=#{smart_install_dir}/embedded"
    ]

    # jemallocs page size must be >= to the runtime pagesize
    # Use large for arm/newer platforms based on debian rules:
    # https://salsa.debian.org/debian/jemalloc/-/blob/c0a88c37a551be7d12e4863435365c9a6a51525f/debian/rules#L8-23
    autogen_command << (arm64? ? "--with-lg-page=16" : "--with-lg-page=12")

    command(autogen_command.join(" "), env: env)
    command("make -j 4 install", env: env)
  end

  def centos6?
    /centos/.matches?(platform.family || "") && /^6/.matches?(platform.version || "")
  end

  def configure: Nil
    version("5.3.0")
    license("jemalloc")
    source("https://github.com/jemalloc/jemalloc/archive/refs/tags/#{version}.tar.gz")
    relative_path("jemalloc-#{version}")
    preserve_symlinks(false)
    project.exclude("embedded/bin/jemalloc-config")
  end
end
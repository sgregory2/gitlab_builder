@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::GrafanaSimple < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  # DO NOT DISTRIBUTE THIS
  # This is a temporary task to be able to build gitlab
  # while investigating why building grafana from source
  # fails on `make node_modules`
  # This task just pulls in an already built grafana release.
  # 
  # Tasks::Grafana applies a security patch that this does not.

  @@name = "grafana"

  def build : Nil
    mkdir("#{smart_install_dir}/embedded/bin", parents: true)
    mkdir("#{smart_install_dir}/embedded/service/grafana/public", parents: true)
    mkdir("#{smart_install_dir}/embedded/service/grafana/conf", parents: true)

    copy("bin/grafana-server", "#{smart_install_dir}/embedded/bin/grafana-server")
    copy("bin/grafana-cli", "#{smart_install_dir}/embedded/bin/grafana-cli")

    sync("public", "#{smart_install_dir}/embedded/service/grafana/public")
    copy("conf/defaults.ini", "#{smart_install_dir}/embedded/service/grafana/conf")
  end

  def sha
   {
    "amd64" => "78491e7c07a3f2d810a55256df526d66b7f3d6ee3628ae5ab3862b81838d7852",
    "arm64" => "9c7ed2c9ccd2fe4844bd8e02cc11beb9eedd87345678fc944e55610249e83cb4",
    "armv7" => "95039bd8c239b93819a381b37bcdf6c3d5515b189b748ea84bee49b692be38c7"
   }[arch]
  end

  def arch
    if raspberry_pi?
      "armv7"
    elsif /aarch64/.matches?(kernel.machine || "")
      "arm64"
    else
      "amd64"
    end
  end

  def configure: Nil
    license("AGPLv3") # omnibus says APACHE-2.0
    version("7.5.16")
    source("https://dl.grafana.com/oss/release/grafana-#{version}.linux-#{arch}.tar.gz", sha256: sha)
    preserve_symlinks(false)
  end
end
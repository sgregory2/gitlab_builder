@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::NodeExporter < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "node-exporter"

  def build : Nil
    env = {
      "GOPATH" => "#{project.source_dir}/node_exporter",
      "CGO_ENABLED" => "0", # Details: https://github.com/prometheus/node_exporter/issues/870
      "GO111MODULE" => "on"
    }

    mkdir("#{smart_install_dir}/embedded/bin", parents: true)
    command("go build -ldflags '#{context.prometheus_flags.ldflags(version)}'", env: env)
    copy("node_exporter", "#{smart_install_dir}/embedded/bin")

    license_finder
  end

  # TODO: fill this out
  def license_finder
  end

  def configure: Nil
    license("APACHE-2.0")
    version("1.4.0")
    source("https://github.com/prometheus/node_exporter/archive/refs/tags/v#{version}.tar.gz")
    relative_path("src/github.com/prometheus/node_exporter")
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::GitlabRailsWorkhorse < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-rails-workhorse"

  dependency GitlabRails

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
    workhorse_flags = use_system_ssl? ? " FIPS_MODE=1" : ""
    command("make install -C workhorse PREFIX=#{install_dir}/embedded#{workhorse_flags}", chdir: rails_dir)
  end

  def rails_dir
    File.join(project.source_dir, "gitlab-rails")
  end
  
  def configure : Nil
    cache(false)
    virtual(true)
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Ohai < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "ohai"

  dependency Ruby
  sequence ["Gem"]

  file("license_patch", "#{__DIR__}/../patches/ohai/license/add-license-file.patch")
  file("notice_patch", "#{__DIR__}/../patches/ohai/license/add-notice-file.patch")

  def build : Nil
    patch(file("license_patch"), string: true)
    patch(file("notice_patch"), string: true)

    env = with_standard_compiler_flags(with_embedded_path)

    # Install a known good version of chef-config to workaround
    # https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/7267
    bin("gem", "install chef-config --version '17.10.19' --no-document", env: env)

    bin("gem", "install ohai --version '#{version}' --bindir '#{install_dir}/embedded/bin' --no-document", env: env)
  end

  def configure: Nil
    version("17.9.0")
    license("Apache-2.0")
    cache(false)
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::SpamClassifier < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "spam-classifier"

  def build : Nil
    mkdir(target, parents: true)
    mkdir("#{target}/preprocessor", parents: true)

    sync(".", target)
    copy("tokenizer.pickle", "#{target}/preprocessor/tokenizer.pickle")
  end

  def target
    File.join(smart_install_dir, "embedded", "service", "spam-classifier")
  end

  def configure: Nil
    license("proprietary")
    version("0.3.0")
    source("https://glsec-spamcheck-ml-artifacts.storage.googleapis.com/spam-classifier/#{version}/linux.tar.gz",
      strip: 0,
      sha256: "c9f7e147d195a45e32c35765e138e006e7636218f8c4413e67d0cef5513335a8")
  end
end
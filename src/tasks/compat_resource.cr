@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
# Seems to be deprecated: https://github.com/chef-boneyard/compat_resource
class Tasks::CompatResource < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "compat-resource"

  def build : Nil
    sync(".", target_path)
  end

  def configure : Nil
    version("12.19.1")
    license("Apache-2.0")
    source("https://github.com/chef-boneyard/compat_resource/archive/refs/tags/v#{version}.tar.gz")
  end

  def target_path
    File.join(smart_install_dir, "embedded", "cookbooks", "compat_resource")
  end
end
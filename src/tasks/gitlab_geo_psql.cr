@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
class Tasks::GitlabGeoPsql < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-geo-psql"

  def build : Nil
    mkdir(File.join(smart_install_dir, "bin"), parents: true)

    block do
      File.open(File.join(smart_install_dir, "bin", "gitlab-geo-psql"), "w") do |file|
        file.print(script)
      end
    end
  end

  def script
    str = <<-EOH
      #!/bin/sh

      error_echo()
      {
        echo "$1" 2>& 1
      }

      gitlab_geo_psql_rc='/opt/gitlab/etc/gitlab-geo-psql-rc'


      if ! [ -f ${gitlab_geo_psql_rc} ] ; then
        error_echo "$0 error: could not load ${gitlab_geo_psql_rc}"
        error_echo "Either you are not allowed to read the file, or it does not exist yet."
        error_echo "You can generate it with:   sudo gitlab-ctl reconfigure"
        exit 1
      fi

      . ${gitlab_geo_psql_rc}

      if [ "$(id -n -u)" = "${psql_user}" ] ; then
        privilege_drop=''
      else
        privilege_drop="-u ${psql_user}:${psql_group}"
      fi

      cd /tmp; exec /opt/gitlab/embedded/bin/chpst ${privilege_drop} -U ${psql_user} /usr/bin/env PGSSLCOMPRESSION=0 /opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} -d ${psql_dbname} "$@"
    EOH

    str
  end

  def configure : Nil
    license("Apache-2.0")
    version(Digest::MD5.new.file(__FILE__).final.hexstring)
  end
end
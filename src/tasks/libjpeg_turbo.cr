@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::LibjpegTurbo < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libjpeg-turbo"

  dependency Zlib

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    command(config_command.join(" "), env: env)
    command("make install", env: env)
  end

  def config_command
    [
      "cmake",
      "-G\"Unix Makefiles\"",
      "-DCMAKE_INSTALL_LIBDIR:PATH=lib", # ensure lib64 isn't used
      "-DCMAKE_INSTALL_PREFIX=#{install_dir}/embedded"
    ]
  end

  def configure: Nil
    license("BSD-3-Clause")
    version("2.1.2")
    source("https://github.com/libjpeg-turbo/libjpeg-turbo/archive/refs/tags/#{version}.tar.gz")
  end
end
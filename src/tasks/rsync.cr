@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Rsync < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "rsync"

  dependency Popt

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    command([
      "./configure",
      "--prefix=#{install_dir}/embedded",
      "--disable-iconv",
      "--disable-xxhash",
      "--disable-zstd",
      "--disable-lz4"
    ].join(" "), env: env)

    command("make", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    license("GPL v3")
    version("3.2.7")
    source("https://rsync.samba.org/ftp/rsync/src/rsync-#{version}.tar.gz",
      sha256: "4e7d9d3f6ed10878c58c5fb724a67dacf4b6aac7340b13e488fb2dc41346f2bb")
  end
end
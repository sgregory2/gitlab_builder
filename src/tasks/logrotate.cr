@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Logrotate < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "logrotate"

  dependency Popt

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    command("./autogen.sh", env: env)
    command("./configure --prefix=#{install_dir}/embedded --without-selinux", env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    license("GPL-2.0")
    version("3.18.0")
    source("https://github.com/logrotate/logrotate/archive/refs/tags/#{version}.tar.gz")
  end
end
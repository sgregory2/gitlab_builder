@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::NginxModuleVts < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "nginx-module-vts"

  file("fix_compile_error", "#{__DIR__}/../patches/nginx-module-vts/fix-compile-errors-in-gcc-11.patch")

  def build : Nil
    patch(file("fix_compile_error"), string: true)
  end

  def configure: Nil
    version("0.1.18")
    source("https://github.com/vozlt/nginx-module-vts/archive/refs/tags/v#{version}.tar.gz")
    license("BSD-2-Clause")
    cache(false)
  end
end
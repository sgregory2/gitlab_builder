
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::AcmeClient < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "acme-client"

  dependency Ruby
  sequence ["Gem"]

  file("license_patch", "#{__DIR__}/../patches/acme-client/add-license-file.patch")

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)

    patch(file("license_patch"), string: true)
    bin("gem", "install acme-client --no-document --version #{version}", env: env)
  end

  # TODO: use license_file from patch\
  def configure : Nil
    version("2.0.11")
    license("MIT")
    cache(false)
  end
end
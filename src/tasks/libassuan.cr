@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Libassuan < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libassuan"

  dependency LibgpgError

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    command("./configure --prefix=#{install_dir}/embedded --disable-doc", env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    version("2.5.3")
    license("LGPL-2.1")
    source("https://www.gnupg.org/ftp/gcrypt/libassuan/libassuan-#{version}.tar.bz2", 
      sha256: "91bcb0403866b4e7c4bc1cc52ed4c364a9b5414b3994f718c70303f7f765e702")
    relative_path("libassuan-#{version}")
    project.exclude("embedded/bin/libassuan-config")
  end
end
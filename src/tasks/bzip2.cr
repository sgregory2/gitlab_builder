@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Bzip2 < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers
  
  @@name = "bzip2"

  file("env_vars_patch", "#{__DIR__}/../patches/bzip2/makefile_take_env_vars.patch")

  dependency Zlib
  dependency OpenSSL unless use_system_ssl?

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    env["CFLAGS"] += " -fPIC"

    emit("env: #{env.to_s}, smart_install: #{smart_install_dir}")
    patch(file("env_vars_patch"), string: true, env: env)
    command("make PREFIX='#{install_dir}/embedded' VERSION='#{version}'", env: env)
    command("make PREFIX='#{install_dir}/embedded' VERSION='#{version}' -f Makefile-libbz2_so", env: env)
    command("make PREFIX='#{install_dir}/embedded' VERSION='#{version}' install", env: env)
  end

  def configure : Nil
    version("1.0.8")
    license("BSD-2-Clause")
    relative_path("#{name}-#{version}")
    source("https://sourceware.org/pub/bzip2/bzip2-#{version}.tar.gz",
      sha512: "083f5e675d73f3233c7930ebe20425a533feedeaaa9d8cc86831312a6581cefbe6ed0d08d2fa89be81082f2a5abdabca8b3c080bf97218a1bd59dc118a30b9f3")
  end
end
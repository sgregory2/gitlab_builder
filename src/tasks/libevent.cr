@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Libevent < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libevent"

  dependency Libtool
  dependency OpenSSL unless use_system_ssl?

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    env["ACLOCAL_PATH"] = File.join(install_dir, "embedded", "share", "aclocal")

    command("./autogen.sh", env: env)
    command("./configure --prefix=#{install_dir}/embedded", env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    license("BSD-3-Clause")
    # omnibus uses 2.1.8
    version("2.1.12")
    source("https://github.com/libevent/libevent/releases/download/release-#{version}-stable/libevent-#{version}-stable.tar.gz")
  end
end
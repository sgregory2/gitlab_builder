@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Libtiff < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libtiff"

  file("config_guess_patch", "#{__DIR__}/../patches/libtiff/remove-config-guess-sub-download.patch")

  dependency Libtool
  dependency Zlib
  dependency Liblzma
  dependency LibjpegTurbo
  dependency ConfigGuess

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    
    if platform.family == "centos" && platform.version.try(&.starts_with?("6"))
      cmd = centos_config
    else
      cmd = rest_config(env)
    end
    
    command(cmd.join(" "), env: env)
    command("make install", env: env)
  end

  # Patch the code to download config.guess and config.sub. We instead copy
  # the ones we vendor to the correct location.
  def rest_config(env)
    patch(file("config_guess_patch"), string: true)
    command("./autogen.sh", env: env)
    update_config_guess(target: "config")

    [
      "./configure",
      "--disable-zstd",
      "--prefix=#{install_dir}/embedded"
    ]
  end

  # Use cmake for CentOS 6 builds
  # CentOS 6 doesn't have a new enough version of automake, so we need to use
  # the cmake build steps, but the cmake steps aren't working properly in
  # Debian 8, which is why we don't just switch to cmake for all platforms
  def centos_config
    [
      "cmake",
      "-G\"Unix Makefiles\"",
      "-Dzstd=OFF",
      "-DZLIB_ROOT=#{install_dir}/embedded",
      "-DCMAKE_INSTALL_LIBDIR:PATH=lib", # ensure lib64 isn't used
      "-DCMAKE_INSTALL_RPATH=#{install_dir}/embedded/lib",
      "-DCMAKE_FIND_ROOT_PATH=#{install_dir}/embedded",
      "-DCMAKE_PREFIX_PATH=#{install_dir}/embedded",
      "-DCMAKE_INSTALL_PREFIX=#{install_dir}/embedded"
    ]
  end

  def configure: Nil
    version("4.4.0")
    license("libtiff")
    source("https://download.osgeo.org/libtiff/tiff-#{version}.tar.xz")
  end
end
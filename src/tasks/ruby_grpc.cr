@[Barista::BelongsTo(GitlabFIPS)]
class Tasks::RubyGrpc < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "ruby-grpc"

  file("grpc_patch", "#{__DIR__}/../patches/ruby-grpc/grpc-system-ssl.patch")
    
  dependency Ruby
  sequence ["Gem"]

  def build : Nil
    block "re-install grpc gem with system OpenSSL" do
      versions = [] of String

      bin("ruby", "-e \"puts Gem::Specification.select { |x| x.name == 'grpc' }.map(&:version).uniq.map(&:to_s)\"")
        .collect_output(versions)
        .forward_error(&on_error)
        .execute

      versions = versions.map(&.strip)
      raise "No gRPC versions installed, failing build" if versions.empty?

      bin("gem", "install --no-document gem-patch -v 0.1.6").forward_output(&on_output).forward_error(&on_error).execute
      bin("gem", "uninstall --force --all grpc").forward_output(&on_output).forward_error(&on_error).execute

      file = write_patch_file(file("grpc_patch"))

      versions.each do |version|
        gemfile = "grpc-#{version}.gem"
        command("rm -f #{gemfile}").forward_output(&on_output).forward_error(&on_error).execute
        bin("gem", "fetch grpc -v #{version} --platform ruby").forward_output(&on_output).forward_error(&on_error).execute
        bin("gem", "patch -p1 #{gemfile} #{file.path}").forward_output(&on_output).forward_error(&on_error).execute
        bin("gem", "install --platform ruby --no-document #{gemfile}").forward_output(&on_output).forward_error(&on_error).execute
      end

      bin("gem", "uninstall gem-patch").forward_output(&on_output).forward_error(&on_error).execute
      file.try(&.delete)
    end
  end

  protected def write_patch_file(patch_file_string)
    File.tempfile("patch") do |f|
      f << patch_file_string
    end
  end

  def configure: Nil
    version("0.0.1")
    license(project.license)
    cache(false)
  end
end
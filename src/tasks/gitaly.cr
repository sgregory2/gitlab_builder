@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Gitaly < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitaly"
  @ruby_install_dir : String?
  
  dependency PkgConfigLite
  dependency Ruby
  dependency Bundler
  dependency Libicu
  dependency Git
  sequence ["Gem"]

  file("hooks_wrapper", "#{__DIR__}/../templates/gitaly/gitlab_shell_hooks_wrapper.hbs")

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
    env["SSL_CERT_FILE"] = "#{install_dir}/embedded/ssl/cert.pem"
    handle_system_ssl_env(env)
    
    bin("bundle",  "config set --local frozen 'true'", env: env)
    bin("bundle", build_config, env: env)
    bin("bundle", "install --without #{bundle_without.join(" ")}", env: env, chdir: ruby_build_dir)
    command("touch .ruby-bundle")
    
    # TODO: this depends on a #{install_dir}/embedded/bin/license_finder 
    # binary, which we don't have yet.
    # find_gem_licenses(env)

    # From Omnibus GitLab
    # https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/config/software/gitaly.rb#L66
    # TODO: find a simpler way to do this.
    block "delete grpc shared objects" do
      command("find #{File.join(grpc_path(env), "src/ruby/lib/grpc")} ! -path '*/#{ruby_version(env)}/*' -name 'grpc_c.so' -type f -print -delete")
        .forward_output(&on_output)
        .forward_error(&on_error)
        .execute
    end

    sync("./ruby", ensure_ruby_install_dir, exclude: [".git", ".gitignore", "spec", "features"])

    %w[LICENSE NOTICE VERSION].each { |f| copy(f, ensure_ruby_install_dir) }

    command("make install PREFIX=#{install_dir}/embedded", env: env)

    # TODO: find where these are being populated.
    # block "disable RubyGems in gitlab-shell hooks" do
    #   hooks.each do |ruby_script|
    #     script = File.read(ruby_script)

    #     template(
    #       src: file("hooks_wrapper"), 
    #       dest: ruby_script.gsub(hooks_source_dir, hooks_dest_dir), 
    #       mode: File::Permissions.new(0o755),
    #       vars: { "script" => script, "install_dir" => install_dir },
    #       string: true
    #     )
    #   end
    # end

    # TODO: more license finder reporting
    # https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/config/software/gitaly.rb#L97
    block "Merge license files of ruby and go dependencies of Gitaly" { }
  end

  def hooks
    output = [] of String
    command("grep -r -l '^#!/usr/bin/env ruby' #{hooks_source_dir}")
      .collect_output(output)
      .forward_error(&on_error)
      .execute

     output 
  end

  def hooks_source_dir
    File.join(ruby_build_dir, "gitlab-shell", "hooks")
  end

  def hooks_dest_dir
    File.join(ensure_ruby_install_dir, "gitlab-shell", "hooks")
  end

  def ensure_ruby_install_dir : String
    @ruby_install_dir ||= begin
      dir = File.join(install_dir, "embedded", "service", "gitaly-ruby")
      mkdir(dir, parents: true)
      dir
    end
  end

  def grpc_path(env)
    lines = [] of String
    bin("bundle", "show grpc", env: env, chdir: ruby_build_dir)
      .collect_output(lines)
      .execute

    lines.last.try(&.strip)
  end

  def ruby_build_dir
    File.join(source_dir, "ruby")
  end

  def bundle_without
    %w[development test]
  end

  def build_config
    String.build do |io|
      io << "config build.nokogiri "
      io << "--use-system-libraries "
      io << "--with-xml2-include=#{install_dir}/embedded/include/libxml2 "
      io << "--with-xslt-include=#{install_dir}/embedded/include/libxslt"
    end
  end

  # TODO: Implement OpenSSL Helper
  # https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/lib/gitlab/openssl_helper.rb
  def handle_system_ssl_env(env : Hash(String, String))
    if use_system_ssl?
      raise "Not Implemented"
    end
  end

  def find_gem_licenses(env)
    str = String.build do |io|
      io << "exec license_finder "
      io << "report --decisions-file=#{File.join(project.root, "support", "dependency_decisions.yml")} "
      io << "--format=json --columns name version licenses texts notice "
      io << "--save=gitaly-ruby-licenses.json"
    end
    bin("bundle", str, chdir: ruby_build_dir, env: env)
  end

  def configure : Nil
    license("MIT")
    cache(false)
    version(context.version.gitaly_version)
    source("https://gitlab.com/gitlab-org/gitaly/-/archive/#{version}/gitaly-#{version}.tar.gz")
    #license_file("LICENSE")
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Libpng < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libpng"

  dependency Zlib

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    command("./configure --prefix=#{install_dir}/embedded --with-zlib=#{install_dir}/embedded", env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    version("1.6.38")
    license("Libpng")
    source("https://sourceforge.net/projects/libpng/files/libpng16/#{version}/libpng-#{version}.tar.gz/download", extension: ".tar.gz")
  end
end
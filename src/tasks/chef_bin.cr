@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::ChefBin < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  file("license_patch", "#{__DIR__}/../patches/chef-bin/add-license-file.patch")

  dependency Ruby
  dependency Libffi
  sequence ["Gem"]

  @@name = "chef-bin"

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
    env["SSL_CERT_FILE"] = "#{install_dir}/embedded/ssl/cert.pem"

    patch(file("license_patch"), string: true)
    
    cmd = String.build do |io|
      io << "install chef-bin --clear-sources "
      io << "-s https://packagecloud.io/cinc-project/stable "
      io << "-s https://rubygems.org "
      io << "--version '#{version}' "
      io << "--bindir '#{install_dir}/embedded/bin' "
      io << "--no-document"
    end

    bin("gem", cmd, env: env)
  end

  # TODO: specify license file
  def configure : Nil
    version("17.10.0")
    license("Apache-2.0")
    cache(false)
  end
end

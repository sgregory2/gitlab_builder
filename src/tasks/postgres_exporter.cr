@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::PostgresExporter < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "postgres-exporter"

  def build : Nil
    env = { "GOPATH" => "#{project.source_dir}/postgres_exporter" }
  
    mkdir("#{smart_install_dir}/embedded/bin", parents: true)
    command("go build -ldflags '#{context.prometheus_flags.ldflags(version)}' ./cmd/postgres_exporter", env: env)  
    copy("postgres_exporter", "#{smart_install_dir}/embedded/bin/postgres_exporter")

    license_finder
  end

  def license_finder
  end

  def configure: Nil
    license("Apache-2.0")
    version("0.11.1")
    source("https://github.com/prometheus-community/postgres_exporter/archive/refs/tags/v#{version}.tar.gz")
    relative_path("src/github.com/wrouesnel/postgres_exporter")
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::LibosspUuid < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libossp-uuid"

  dependency ConfigGuess

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)

    update_config_guess

    command("./configure --prefix=#{smart_install_dir}/embedded", env: env)
    command("make -j 3", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    license("MIT")
    version("1.6.2")
    source("https://www.mirrorservice.org/sites/ftp.ossp.org/pkg/lib/uuid/uuid-#{version}.tar.gz",
      sha256: "11a615225baa5f8bb686824423f50e4427acd3f70d394765bdff32801f0fd5b0")
    preserve_symlinks(false)
    project.exclude("embedded/bin/uuid-config")
  end
end
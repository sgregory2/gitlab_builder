@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Patroni < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "patroni"
  
  file("license_patch", "#{__DIR__}/../patches/patroni/add-license-file.patch")

  dependency Python3
  dependency Psycopg2

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)

    patch(file("license_patch"), string: true)

    # Version 1.0 of PrettyTable does not work with Patroni 1.6.4
    # https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5701
    bin("pip3", "install prettytable==0.7.2", env: env)
    bin("pip3", "install patroni[consul]==#{version}", env: env)
  end

  def configure: Nil
    license("MIT")
    version("2.1.0")
    cache(false)
  end
end
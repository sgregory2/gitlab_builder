@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::GitlabCookbooks < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers
  
  # packages default appears to be unused
  # file("cookbooks_packages_default", "#{__DIR__}/../templates/gitlab-cookbooks/cookbook_packages_default.hbs")
  file("dna", "#{__DIR__}/../templates/gitlab-cookbooks/dna.json.hbs")
  file("geo-postgresql-config", "#{__DIR__}/../templates/gitlab-cookbooks/geo-postgresql-config.json.hbs")
  file("patroni-config", "#{__DIR__}/../templates/gitlab-cookbooks/patroni-config.json.hbs")
  file("pg-upgrade-config", "#{__DIR__}/../templates/gitlab-cookbooks/pg-upgrade-config.json.hbs")
  file("postgresql-bin", "#{__DIR__}/../templates/gitlab-cookbooks/postgresql-bin.json.hbs")
  file("postgresql-config", "#{__DIR__}/../templates/gitlab-cookbooks/postgresql-config.json.hbs")

  @@name = "gitlab-cookbooks"

  def build : Nil
    cookbook_name = "gitlab"

    mkdir(target, parents: true)
    sync(cookbooks_files, target)

    solo_recipes = %w[dna postgresql-bin postgresql-config pg-upgrade-config]
    if context.version.ee?
      cookbook_name = "gitlab-ee"
      solo_recipes << "geo-postgresql-config"
      solo_recipes << "patroni-config"
    else
      command("rm -r #{install_dir}/embedded/cookbooks/gitlab-ee")
    end

    solo_recipes.each do |config|
      template(
        src: file(config), 
        dest: File.join(install_dir, "embedded", "cookbooks", "#{config}.json"),
        mode: File::Permissions.new(0o755),
        vars: { "cookbook_name" => cookbook_name } ,
        string: true
      )
    end
  end
  
  def target
    File.join(install_dir, "embedded", "cookbooks")
  end

  def cookbooks_files
    File.join(resources_path, "files", "gitlab-cookbooks")
  end

  def configure : Nil
    cache(false)
    virtual(true)
    license("Apache-2.0")
  end
end
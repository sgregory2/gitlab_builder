@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Pcre < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "pcre"

  dependency Libedit
  dependency Ncurses
  dependency ConfigGuess

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    update_config_guess

    command(config_cmd.join(" "), env: env)
    command("make -j 2", env: env)
    command("make install", env: env)
  end

  def config_cmd
    [
      "./configure",
      "--prefix=#{install_dir}/embedded",
      "--disable-cpp",
      "--enable-utf",
      "--enable-unicode-properties",
      "--enable-pcretest-libedit"
    ]
  end

  def configure: Nil
    version("8.44")
    license("BSD-2-Clause")
    source("http://downloads.sourceforge.net/project/pcre/pcre/#{version}/pcre-#{version}.tar.gz",
      sha256: "aecafd4af3bd0f3935721af77b889d9024b2e01d96b58471bd91a3063fb47728")

    project.exclude("embedded/bin/pcre-config")
  end
end
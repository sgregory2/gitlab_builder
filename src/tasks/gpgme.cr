@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Gpgme < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gpgme"

  dependency Libassuan
  dependency Gnupg
  dependency Zlib

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    env["CFLAGS"] += " -std=c99"
    command("./configure --prefix=#{install_dir}/embedded --disable-doc --disable-languages", env: env)
    command("make -j 3", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    version("1.17.0")
    license("LGPL-2.1")
    source("https://www.gnupg.org/ftp/gcrypt/gpgme/gpgme-#{version}.tar.bz2",
      sha256: "4ed3f50ceb7be2fce2c291414256b20c9ebf4c03fddb922c88cda99c119a69f5")
    relative_path("gpgme-#{version}")
    project.exclude("embedded/bin/gpgme-config")
  end
end
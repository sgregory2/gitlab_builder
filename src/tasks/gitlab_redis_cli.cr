@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::GitlabRedisCli < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-redis-cli"

  def build : Nil
    mkdir(target, parents: true)
    block do
      File.open(File.join(target, name), "w") { |file| file.print(script) }
    end
    command("chmod 755 #{target}/#{name}")
  end

  def target
    File.join(smart_install_dir, "bin")
  end

  def script
    str = <<-EOH
      #!/bin/sh

      error_echo()
      {
        echo "$1" 2>& 1
      }

      gitlab_redis_cli_rc='/opt/gitlab/etc/gitlab-redis-cli-rc'

      if ! [ -f ${gitlab_redis_cli_rc} ] || ! [ -r ${gitlab_redis_cli_rc} ] ; then
        error_echo "$0 error: could not load ${gitlab_redis_cli_rc}"
        error_echo "Either you are not allowed to read the file, or it does not exist yet."
        error_echo "You can generate it with:   sudo gitlab-ctl reconfigure"
        exit 1
      fi

      . "${gitlab_redis_cli_rc}"

      if [ -e "${redis_socket}" ]; then
        REDIS_PARAMS="-s ${redis_socket}"
      else
        REDIS_PARAMS="-h ${redis_host} -p ${redis_port}"
      fi

      REDISCLI_AUTH="$(awk '/^requirepass /{
        pwd = $0 ;
        gsub(/^requirepass /,"",pwd);
        gsub(/^"|"$/, "", pwd);
        print pwd }' ${redis_dir}/redis.conf)"


      if [ -n "${REDISCLI_AUTH}" ]; then
          export REDISCLI_AUTH
      fi

      exec /opt/gitlab/embedded/bin/redis-cli $REDIS_PARAMS "$@"

    EOH
    str
  end

  def configure: Nil
    version(Digest::MD5.new.file(__FILE__).final.hexstring)
    license("Apache-2.0")
  end
end
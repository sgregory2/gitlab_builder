@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Zlib < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "zlib"

  def build : Nil
    env = with_standard_compiler_flags(with_cache_dest)
    env["CFLAGS"] += " -O3"
    env["CFLAGS"] += " -fno-omit-frame-pointer"

    command("./configure --prefix=#{install_dir}/embedded", env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    version("1.2.13")
    license("Zlib")
    source("https://github.com/madler/zlib/archive/refs/tags/v#{version}.tar.gz")
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Npth < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "npth"

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    command("./configure --prefix=#{install_dir}/embedded --disable-doc", env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def configure: Nil
    version("1.6")
    license("LGPL-2.1")
    source("https://www.gnupg.org/ftp/gcrypt/npth/npth-#{version}.tar.bz2",
      sha256: "1393abd9adcf0762d34798dc34fdcf4d0d22a8410721e76f1e3afcd1daa4e2d1")
    
    project.exclude("embedded/bin/npth-config")
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Runit < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "runit"

  file("log_status_patch", "#{__DIR__}/../patches/runit/log-status.patch")
  file("runsvdir_start", "#{__DIR__}/../templates/runit/runsvdir-start.hbs")

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
    
    # Patch runit to not consider status of log service associated with a service
    # on determining output of status command. For details, check
    # https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4008
    patch(file("log_status_patch"), string: true, chdir: runit_src_dir)

    block "modify sv.c" do
      data = File.read("#{runit_src_dir}/sv.c")
      data = data.gsub(/varservice\s*=\s*\"\/service/, "varservice = \"#{install_dir}/service")
      File.write("#{runit_src_dir}/sv.c", data)
    end

    # TODO: the following is not idempotent
    command("sed -i -e s:-static:: Makefile", env: env, chdir: runit_src_dir)
    command("make", env: env, chdir: runit_src_dir)
    command("make check", env:  env, chdir: runit_src_dir)

    mkdir(target, parents: true)
    %w[chpst runit runit-init runsv runsvchdir runsvdir sv svlogd utmpset].each do |binary|
      copy("#{runit_src_dir}/#{binary}", target)
    end

    template(
      src: file("runsvdir_start"),
      dest: File.join(smart_install_dir, "embedded", "bin", "runsvdir-start"),
      mode: File::Permissions.new(0o755),
      vars: { "install_dir" => install_dir, "current_year" => Time.utc.year.to_s, "log" => "." * 395 },
      string: true
    )

    # setup service directories
    %w[service sv init].each do |dir|
      mkdir("#{smart_install_dir}/#{dir}", parents: true)
      command("touch #{smart_install_dir}/#{dir}/.gitkeep")
    end
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def runit_src_dir
    File.join(source_dir, "src")
  end

  def configure: Nil
    version("2.1.2")
    license("BSD-3-Clause")
    source("http://smarden.org/runit/runit-#{version}.tar.gz", strip: 2, 
      sha256: "6fd0160cb0cf1207de4e66754b6d39750cff14bb0aa66ab49490992c0c47ba18") 
  end
end
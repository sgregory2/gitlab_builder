@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::MailRoom < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "mail_room"
  
  dependency Ruby
  sequence ["Gem"]

  file("license_patch", "#{__DIR__}/../patches/mail_room/add-license-file.patch")

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
    patch(file("license_patch"), string: true)

    bin("gem", "install gitlab-mail_room --no-document --version #{version}", env: env)
  end

  def configure: Nil
    version("0.0.20")
    license("MIT")
    cache(false)
  end
end
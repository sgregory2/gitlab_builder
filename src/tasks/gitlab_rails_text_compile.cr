@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::GitlabRailsTextCompile < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  dependency GitlabRailsBundle
  dependency GitlabRailsYarn

  @@name = "gitlab-rails-text-compile"

  def build : Nil
    bin("bundle", "exec rake gettext:compile", env: assets_compile_env, chdir: rails_dir)
  end

  def rails_dir
    File.join(project.source_dir, "gitlab-rails")
  end

  def assets_compile_env : Hash(String, String)
    env = { 
      "NODE_ENV" => "production",
      "RAILS_ENV" => "production",
      "PATH" => "#{install_dir}/embedded/bin:#{ENV["PATH"]}",
      "USE_DB" => "false",
      "SKIP_STORAGE_VALIDATION" => "true",
      "NODE_OPTIONS" => "--max_old_space_size=#{max_old_space_size}"
    }
    env["NO_SOURCEMAPS"] = "true" if no_sourcemaps?
    env
  end

  def max_old_space_size
    "3584"
  end

  def configure : Nil
    cache(false)
    virtual(true)
  end
end
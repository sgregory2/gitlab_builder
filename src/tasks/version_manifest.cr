@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::VersionManifest < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "version-manifest"

  def build : Nil
    block do
      File.write("#{install_dir}/version-manifest.json", project.manifest_json)
    end
  end

  def configure: Nil
    cache(false)
    version("0.0.1")
    license(project.license)
  end
end
@[Barista::BelongsTo(GitlabTest)]
class HelloWorld < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers
  
  @@name = "hello-world"

  def build : Nil
    emit("Hello world")
    emit("#{name} - #{context.version.gitlab_version}")
    command("echo \"Hello world\" > test.txt", chdir: install_dir)
    emit("Rasberry Pi? #{raspberry_pi?}")
    emit("Arm64? #{arm64?}")
  end

  def configure : Nil
  end
end
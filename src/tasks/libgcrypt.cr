@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Libgcrypt < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libgcrypt"

  dependency LibgpgError

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    
    flags = ["--prefix=#{install_dir}/embedded", "--disable-doc"]
    add_rasberry_pi_flags(flags)

    command("./configure #{flags.join(" ")}", env: env)
    command("make", env: env)
    command("make install", env: env)
  end

  def add_rasberry_pi_flags(flags)
    if raspberry_pi?
      %w[host build].each do |w|
        flags << "--#{w}=#{gcc_target}"
      end
    end
  end

  def configure: Nil
    version("1.9.4")
    license("LGPL-2.1")
    source("https://www.gnupg.org/ftp/gcrypt/libgcrypt/libgcrypt-#{version}.tar.bz2",
      sha256: "ea849c83a72454e3ed4267697e8ca03390aee972ab421e7df69dfe42b65caaf7")
    # is this needed?
    relative_path("libgcrypt-#{version}")
    project.exclude("embedded/bin/libgcrypt-config")
  end
end
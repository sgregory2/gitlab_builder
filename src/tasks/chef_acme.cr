@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::ChefAcme < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  dependency GitlabCookbooks
  dependency AcmeClient
  dependency CompatResource

  @@name = "chef-acme"

  def build : Nil
    sync(".", target_path)
  end

  def target_path
    File.join(smart_install_dir, "embedded", "cookbooks", "acme")
  end

  # TODO: specify license file
  def configure : Nil
    version("4.1.5")
    license("Apache-2.0")
    source("https://github.com/schubergphilis/chef-acme/archive/refs/tags/v#{version}.tar.gz")
  end
end

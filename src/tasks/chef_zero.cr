@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::ChefZero < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  file("license_file", "#{__DIR__}/../patches/chef-zero/license/add-license-file.patch")

  dependency Ruby
  sequence ["Gem"]

  # From Barista GitLab
  # dependency FFIGem
  # dependency GemUpdate
  # dependency ChefBin

  @@name = "chef-zero"

  def build : Nil
    patch(file("license_file"), string: true)

    env = with_standard_compiler_flags(with_embedded_path)
    env["SSL_CERT_FILE"] = File.join(install_dir, "embedded", "ssl", "cert.pem")

    cmd = String.build do |io|
      io << "install chef-zero --clear-sources "
      io << "-s https://packagecloud.io/cinc-project/stable "
      io << "-s https://rubygems.org "
      io << "--version '#{version}' "
      io << "--bindir '#{install_dir}/embedded/bin' "
      io << "--no-document"
    end

    bin("gem", cmd, env: env)
  end

  # TODO: specify license/notice files
  def configure : Nil
    version("15.0.11")
    license("Apache-2.0")
    cache(false)
  end
end

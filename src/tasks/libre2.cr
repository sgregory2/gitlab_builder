@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Libre2 < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libre2"

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    command("make", env: env)
    command("make install prefix=#{install_dir}/embedded", env: env)
  end

  def configure: Nil
    version("2016-02-01")
    source("https://github.com/google/re2/archive/refs/tags/#{version}.tar.gz")
    license("BSD")
  end
end
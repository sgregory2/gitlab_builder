@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
class Tasks::GitlabElasticsearchIndexer < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-elasticsearch-indexer"

  dependency Libicu

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))

    command("make install PREFIX=#{install_dir}/embedded", env: env)
    handle_licensing
  end

  # TODO: use a icense finder to resolve licenses
  # https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/master/config/software/gitlab-elasticsearch-indexer.rb#L36
  def handle_licensing
  end

  def configure : Nil
    license("MIT")
    version(context.version.gitlab_elasticsearch_indexer_version)
    source("https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer/-/archive/#{version}/gitlab-elasticsearch-indexer-#{version}.tar.gz")
  end
end
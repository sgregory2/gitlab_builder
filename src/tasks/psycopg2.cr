@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Psycopg2 < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "psycopg2"

  dependency Python3
  dependency PostgresqlNew

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
    env["PATH"] = "#{install_dir}/embedded/postgresql/#{pg_major_version}/bin:#{env["PATH"]}"

    bin("python3", "setup.py build_ext", env: env)
    bin("python3", "setup.py install", env: env)
  end

  def pg_major_version
    "13"
  end

  def configure: Nil
    cache(false)
    license("LGPL")
    version("2_8_6")
    source("https://github.com/psycopg/psycopg2/archive/refs/tags/#{version}.tar.gz")
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::RemoteSyslog < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "remote-syslog"

  file("license_patch", "#{__DIR__}/../patches/remote-syslog/license/1.6.15/add-license-file.patch")

  dependency Ruby
  sequence ["Gem"]

  def build : Nil
    patch(file("license_patch"), string: true)
    env = with_standard_compiler_flags(with_embedded_path)

    bin("gem", "install remote_syslog -n #{install_dir}/embedded/bin --no-document -v #{version}", env: env)
  end

  def configure: Nil
    cache(false)
    license("MIT")
    version("1.6.15")
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::RedisExporter < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "redis-exporter"
  
  def build : Nil
    env = { "GOPATH" => "#{project.source_dir}/redis-exporter", "GO111MODULE" => "on" }

    command("go build -ldflags '#{ldflags.join(" ")}'", env: env)
    mkdir(target, parents: true)
    copy("redis_exporter", target)

    license_finder
  end

  # TODO: implement
  def license_finder
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end
  
  def ldflags
    [
      "-X main.BuildVersion=#{version}",
      "-X main.BuildDate=''",
      "-X main.BuildCommitSha=''",
      "-s",
      "-w"
    ]
  end

  def configure: Nil
    license("MIT")
    relative_path("src/github.com/oliver006/redis_exporter")
    version("1.44.0")
    source("https://github.com/oliver006/redis_exporter/archive/refs/tags/v#{version}.tar.gz")
  end
end
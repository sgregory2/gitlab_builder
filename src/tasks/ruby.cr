@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Ruby < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "ruby"

  file("mkmf_patch", "#{__DIR__}/../patches/ruby/ruby-mkmf.patch")
  file("memory_2_7_patch", "#{__DIR__}/../patches/ruby/thread-memory-allocations-2.7.patch")
  file("memory_3_0_patch", "#{__DIR__}/../patches/ruby/thread-memory-allocations-3.0.patch")
  file("disable_copy_range", "#{__DIR__}/../patches/ruby/ruby-disable-copy-file-range.patch")

  dependency Zlib
  dependency OpenSSL unless use_system_ssl?
  dependency Libffi
  dependency Libyaml
  # Needed for chef_gem installs of (e.g.) nokogiri on upgrades -
  # they expect to see our libiconv instead of a system version.
  dependency Libiconv
  dependency Jemalloc

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    # Ruby will compile out the OpenSSL dyanmic checks for FIPS when
    # OPENSSL_FIPS is not defined. RedHat always defines this macro in
    # /usr/include/openssl/opensslconf-x86_64.h, but Ubuntu does not do
    # this.
    env["CFLAGS"] += " -DOPENSSL_FIPS" if use_system_ssl?
    env["CFLAGS"] += " -O3 -g -pipe"

    # Workaround for https://bugs.ruby-lang.org/issues/19161
    env["CFLAGS"] += " -std=gnu99" if sles_7?
    
    env["CFLAGS"] += " -fno-omit-frame-pointer"
    env["LDFLAGS"] += " -Wl,--no-as-needed"

    # disable libpath in mkmf across all platforms, it trolls omnibus and
    # breaks the postgresql cookbook.  i"m not sure why ruby authors decided
    # this was a good idea, but it breaks our use case hard.  AIX cannot even
    # compile without removing it, and it breaks some native gem installs on
    # other platforms.  generally you need to have a condition where the
    # embedded and non-embedded libs get into a fight (libiconv, openssl, etc)
    # and ruby trying to set LD_LIBRARY_PATH itself gets it wrong.
    patch(file("mkmf_patch"), plevel: 1, env: env, string: true)

    if version.starts_with?("2.7")
      patch(file("memory_2_7_patch"), plevel: 1, env: env, string: true)
    elsif version.starts_with?("3.0")
      patch(file("memory_3_0_patch"), plevel: 1, env: env, string: true)
    end

    if (platform.name == "centos" || rhel?) && version.starts_with?("2.7")
      patch(file("disable_copy_range"), plevel: 1, env: env)
    end

    command(configure_command.join(" "), env: env)
    command("make", env:  env)
    command("make install", env: env)
  end

  def configure_command
    cmd = [
      "./configure",
      "--prefix=#{install_dir}/embedded",
      "--with-out-ext=dbm,readline",
      "--enable-shared",
      "--with-jemalloc",
      "--disable-install-doc",
      "--without-gmp",
      "--without-gdbm",
      "--without-tk",
      "--disable-dtrace"
    ]

    cmd << "--with-bundled-md5" if use_system_ssl?
    
    if raspberry_pi?
      %w[host target build].each do |w|
        cmd << "--#{w}=#{gcc_target}"
      end
    end

    cmd << "--with-opt-dir=#{install_dir}/embedded"
    cmd
  end

  def sles_7?
    get_centos_version == "7" || os_platform == "sles"
  end

  def major_minor
    version.match(/^(\d+\.\d+)/).try(&.[0])
  end

  def configure: Nil
    license("BSD-2-Clause")

    if ruby3_build?
      version("3.0.5")
      sha = "9afc6380a027a4fe1ae1a3e2eccb6b497b9c5ac0631c12ca56f9b7beb4848776"
    else
      version("2.7.7")
      sha = "e10127db691d7ff36402cfe88f418c8d025a3f1eea92044b162dd72f0b8c7b90"
    end

    source("https://cache.ruby-lang.org/pub/ruby/#{major_minor}/ruby-#{version}.tar.gz", sha256: sha)
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::GitlabRailsBundle < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "gitlab-rails-bundle"

  dependency GitlabRails
  dependency Gitaly
  sequence ["Gem"]

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path)
    restore_cache

    bundle_without = %w[development test mysql]

    bin("bundle", "config build.gpgme --use-system-libraries", env: env, chdir: rails_dir)
    bin("bundle", nokogiri_config, env: env, chdir: rails_dir)
    bin("bundle", grpc_config, env: env, chdir: rails_dir) if os_platform == "raspbian"
    bin("bundle", "config --global jobs 9", env: env, chdir: rails_dir)
    bin("bundle", "install --prefer-local --without='#{bundle_without.join(" ")}' --jobs #{memory.cpus} --retry 5", env: env, chdir: rails_dir)


    block "correct omniauth-jwt permissions" do
      if show = show_omniauth(env)
        command("chmod -R 664 #{show}")
      end
    end

    block "reinstall google-protobuf gem" do
      unless ruby_native_gems_unsupported?
        current = current_protobuf(env)
        protobuf_version = current[/google-protobuf \((.*)\)/, 1]
        reinstall_protobuf(protobuf_version, env)
      end

      command("find #{File.join(grpc_path(env), "src/ruby/lib/grpc")} ! -path '*/#{ruby_version(env)}/*' -name 'grpc_c.so' -type f -print -delete")
        .forward_output(&on_output)
        .forward_error(&on_error)
        .execute
    end

    save_cache(env)
  end

  def grpc_path(env)
    lines = [] of String
    bin("bundle", "show grpc", chdir: rails_dir).collect_output(lines).execute
    lines.last
  end

  def show_omniauth(env) : String?
    begin
      lines = [] of String
      bin("bundle", "show omniauth-jwt", env: env, chdir: rails_dir)
        .collect_output(lines)
        .execute

      lines.last
    rescue ex : Barista::Behaviors::Software::CommandError
      nil
    end
  end

  def current_protobuf(env)
    lines = [] of String
    bin("bundle", "show | grep google-protobuf", env: env, chdir: rails_dir)
      .collect_output(lines)
      .execute

    lines.last
  end

  def reinstall_protobuf(proto_version, env)
    bin("gem", "uninstall --force google-protobuf", env: env).forward_output(&on_output).forward_error(&on_error).execute
    bin("gem", "install google-protobuf --version #{proto_version} --platform=ruby", env: env).forward_output(&on_output).forward_error(&on_error).execute
  end

  def grpc_config
    String.build do |io|
      io << "config build.grpc "
      io << "--with-ldflags=Wl, --no-as-needed "
      io << "--with-dldflags=-latomic"
    end
  end

  def nokogiri_config
    String.build do |io|
      io << "config build.nokogiri "
      io << "--use-system-libraries "
      io << "--with-xml2-include=#{install_dir}/embedded/include/libxml2 "
      io << "--with-xslt-include=#{install_dir}/embedded/include/libxslt"
    end
  end

  def restore_cache
    if project.cache
      mkdir(gitlab_bundle_cache_dir, parents: true)
      sync(gitlab_bundle_cache_dir, File.join(rails_dir, "vendor")) { |_, _| false }
    end
  end

  def save_cache(env)
    if project.cache
      mkdir(gitlab_bundle_cache_dir, parents: true)
      bin("bundle", "cache", env: env, chdir: rails_dir)
      sync(File.join(rails_dir, "vendor"), gitlab_bundle_cache_dir) { |_, _| false }
    end
  end

  def gitlab_bundle_cache_dir
    File.join("/cache", "gitlab", "gitlab-bundle")
  end

  def rails_dir
    File.join(project.source_dir, "gitlab-rails")
  end

  def configure : Nil
    cache(false)
    virtual(true)
  end
end
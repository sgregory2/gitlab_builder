@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Libicu < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "libicu"

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    env["LD_RPATH"] = File.join(install_dir, "embedded", "lib")

    command(configure_command.join(" "), env: env, chdir: icu_dir)
    command("make", env: env, chdir: icu_dir)
    command("make install", env: env, chdir: icu_dir)
    link("embedded/share/icu/#{version}", "embedded/share/icu/current", chdir: "#{smart_install_dir}")
  end

  def configure_command
    [
      "./runConfigureICU",
      "Linux/gcc",
      "--prefix=#{install_dir}/embedded",
      "--enable-shared",
      "--without-sample"
    ]
  end

  def icu_dir
    File.join(source_dir, "source")
  end

  def configure: Nil
    version("57.1")
    license("MIT")
    source(release)
    project.exclude("embedded/bin/icu-config")
  end

  def release
    major, minor = version.split(".")
    "https://github.com/unicode-org/icu/releases/download/release-#{major}-#{minor}/icu4c-#{major}_#{minor}-src.tgz"
  end
end

@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Alertmanager < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "alertmanager"

  def build : Nil
    env = {
      "GOPATH" => File.join(project.source_dir, "alertmanager"),
      "GO111MODULE" => "on"
    }

    ldflags = context.prometheus_flags.ldflags(version)
    target = File.join(smart_install_dir, "embedded", "bin")

    command("go build -ldflags '#{ldflags}' ./cmd/alertmanager", env: env)
    mkdir(target, parents: true)
    copy("alertmanager", target)
  end

  def configure : Nil
    version("0.23.0")
    license("MIT")
    relative_path(File.join("src", "github.com", "prometheus", "alertmanager"))
    source( "https://github.com/prometheus/alertmanager/archive/refs/tags/v#{version}.tar.gz")
  end
end
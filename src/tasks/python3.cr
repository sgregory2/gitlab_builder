@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Python3 < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "python3"

  # Patches below are a backport of https://github.com/python/cpython/pull/24189
  file("readline_patch", "#{__DIR__}/../patches/python3/readline-3-9.patch")
  file("skip_nis_patch", "#{__DIR__}/../patches/python3/skip-nis-build.patch")


  dependency Libedit
  dependency Ncurses
  dependency Zlib
  dependency OpenSSL if use_system_ssl?
  dependency Bzip2
  dependency Libffi
  dependency Liblzma
  dependency Libyaml

  def build : Nil
    env = with_standard_compiler_flags(with_embedded_path(with_cache_dest))
    env = env.merge(lib_env)

    patch(file("readline_patch"), string: true)
    patch(file("skip_nis_patch"), string: true) if skip_nis?

    str = String.build do |io|
      io << "./configure --prefix=#{install_dir}/embedded "
      io << "--enable-shared "
      io << "--with-readline=editline "
      io << "--with-dbmliborder="
    end

    command(str, env: env)
    command("make", env: env)
    command("make install", env: env)

    cleanup
  end

  def  cleanup
    # pulled from omnibus-gitlab, but not seeing these files generated
    # using -f on these
    command("rm -f #{python_dir}/lib-dynload/dbm.*")
    command("rm -f #{python_dir}/lib-dynload/_sqlite.*")
    
    command("rm -rf #{python_dir}/test")
    command("find #{python_dir} -name '__pycache__' -type d -print -exec rm -r {} +")
  end

  def python_dir
    File.join(smart_install_dir, "embedded", "lib", "python3.9")
  end

  def skip_nis?
    ubuntu_22? || debian_11?
  end

  def ubuntu_22?
    platform.name.try(&.starts_with?("ubuntu")) && platform.version.try(&.starts_with?("22"))
  end

  def debian_11?
    platform.family.try(&.starts_with?("debian")) && platform.version.try(&.starts_with?("11"))
  end

  def lib_env
    {
      "CFLAGS" => "-I#{install_dir}/embedded/include -O3 -g -pipe",
      "LDFLAGS" => "-Wl,-rpath,#{lib_path.join(",-rpath,")} -L#{lib_path.join(" -L")} -I#{install_dir}/embedded/include"
    }
  end

  def lib_path
    [
      "#{install_dir}/embedded/lib",
      "#{install_dir}/embedded/lib64",
      "#{install_dir}/lib",
      "#{install_dir}/lib64",
      "#{install_dir}/libexec"
    ]
  end

  # If bumping from 3.9.x to something higher, be sure to update the following files with the new path:
  # files/gitlab-config-template/gitlab.rb.template
  # files/gitlab-cookbooks/gitaly/recipes/enable.rb
  # files/gitlab-cookbooks/gitlab/attributes/default.rb
  # spec/chef/recipes/gitaly_spec.rb
  # spec/chef/recipes/gitlab-rails_spec.rb
  def configure: Nil
    license("Python-2.0")
    version("3.9.6")
    source("https://www.python.org/ftp/python/#{version}/Python-#{version}.tgz",
      sha256: "d0a35182e19e416fc8eae25a3dcd4d02d4997333e4ad1f2eee6010aadc3fe866")
    project.exclude("embedded/bin/python3*-config")
  end
end
@[Barista::BelongsTo(GitlabFIPS)]
@[Barista::BelongsTo(GitlabEE)]
@[Barista::BelongsTo(GitlabCE)]
class Tasks::Registry < Barista::Task
  include Barista::Behaviors::Omnibus::Task
  include Gitlab::TaskHelpers

  @@name = "registry"

  def build : Nil
    env = {
      "GOPATH" => "#{project.source_dir}/registry",
      "BUILDTAGS" => "include_gcs include_oss"
    }
    
    command("make build", env: env)
    command("make binaries", env: env)

    mkdir(target, parents: true)
    sync("bin", target)

    license_finder
  end

  # TODO: implement
  def license_finder
  end

  def target
    File.join(smart_install_dir, "embedded", "bin")
  end

  def configure: Nil
    license("Apache-2.0")
    version("3.61.0")
    source("https://gitlab.com/gitlab-org/container-registry/-/archive/v#{version}-gitlab/container-registry-v#{version}-gitlab.tar.gz")
    relative_path("src/github.com/docker/distribution")
  end
end
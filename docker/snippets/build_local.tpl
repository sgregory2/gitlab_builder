% 
    if [ -z "$CI" ]; then
%
WORKDIR /usr/src/app

# Build the project
COPY shard.yml shard.lock ./
RUN shards install
COPY src src
COPY .git .git
RUN shards build
RUN mv bin/gitlab-builder /

# Copy assets
RUN mkdir -p /opt/barista/resources

COPY ./src/files /opt/barista/resources/files

# Patches and templates should be compiled into the binary.
# COPY ./src/patches /opt/barista/resources/patches
# COPY ./src/templates /opt/barista/resources/templates

WORKDIR /
%
    fi
%

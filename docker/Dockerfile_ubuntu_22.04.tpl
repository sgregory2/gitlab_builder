FROM ubuntu:22.04 as builder

.INCLUDE ./docker/VERSIONS

ENV TZ="Etc/UTC"

# Install required packages
RUN apt-get update -q \
  && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      build-essential \
      libevent-2.1-7 \
      wget \
      git \
      libc6-dev \
      cmake \
      autoconf \
      bison \
      libyaml-dev \
      tzdata \
      locales \
      curl \
      ca-certificates \
      gnupg \
      pkg-config \
      gcc \
      file \
      libgmp-dev \
      automake \
      autopoint \
      lsb-core \ 
      software-properties-common
      

    # Omnibus deps, pull in as needed
    #   autoconf \
    #   automake \
    #   autopoint \
    #   zlib1g-dev \
    #   byacc \
    #   git \
    #   gcc \
    #   g++ \
    #   libssl-dev \
    #   libyaml-dev \
    #   libffi-dev \
    #   libreadline-dev \
    #   libgdbm-dev \
    #   libncurses5-dev \
    #   make \
    #   bzip2 \
    #   curl \
    #   libcurl4-openssl-dev \
    #   ca-certificates \
    #   openssh-server \
    #   libexpat1-dev \
    #   gettext \
    #   libz-dev \
    #   fakeroot \
    #   ccache \
    #   distcc \
    #   unzip \
    #   tzdata \
    #   apt-transport-https \
    #   gnupg \
    #   pkg-config \
    #   python3

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

.INCLUDE ./docker/snippets/go
.INCLUDE ./docker/snippets/cmake
.INCLUDE ./docker/snippets/gcloud
.INCLUDE ./docker/snippets/node
.INCLUDE ./docker/snippets/yarn
.INCLUDE ./docker/snippets/crystal

FROM ubuntu:22.04

LABEL org.opencontainers.image.authors="GitLab Quality. <quality@gitlab.com>"

COPY --from=builder / /

.INCLUDE ./docker/snippets/build_local.tpl
.INCLUDE ./docker/snippets/docker.tpl